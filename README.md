# Форматирование файлов

Основное форматирование задается с помощью файла [.editorconfig](.editorconfig)

В редакторе / IDE устанавливаем расширение `EditorConfig` и добавляем файл в корень проекта.

PHPStorm - в настройках _File > Settings > Editor > Code Style_ чекбокс `Enable editorconfig` должен быть активным

## Ограничение длины строк:

#### VS Code

Поменять значения в _File > Preferences > Settings_

или добавить в settings.json:

```json
    "editor.rulers": [
        80,
        120
    ],
```

#### PHPStorm

_File > Settings > Editor > Code Style_

Hard wrap at `120` columns

`Wrap on typing` - не активный

Visual guides: `80` columns

#### Sublime

Вставить код в _Preferences > Settings_

```json
    "rulers": [
        80,
        120
    ],
```

# Проверка стиля кода

Устанавливаем PHP_CodeSniffer, варианты установки описаны [здесь](https://github.com/squizlabs/PHP_CodeSniffer#installation)

Добавляем файл [phpcs.xml](phpcs.xml) в корень проекта.

#### VS Code

Устанавливаем расширения phpcs и PHP-CS-Fixer. Настройки автоматически подтянутся из файла.
