# Битрикс

### Теги PHP

В коде PHP НЕОБХОДИМО использовать сокращенные теги `<? ?>` и `<?= ?>`

### Кодировка символов

Для кода PHP НЕОБХОДИМО использовать только UTF-8 без BOM.

### Файлы

Во всех PHP-файлах НЕОБХОДИМО использовать окончания строк LF (`\n`).

Все PHP-файлы НЕОБХОДИМО заканчивать одной пустой строкой.

Закрывающий тэг `?>` НЕОБХОДИМО удалять из файлов, содержащих только PHP.

### Строки

НЕДОПУСТИМО жёстко ограничивать длину строки.

Мягкое ограничение длины строки ДОЛЖНО быть 120 символов; автоматизированные утилиты проверки стиля ДОЛЖНЫ предупреждать о нарушении мягкого ограничения, но не считать это ошибкой.

НЕ СЛЕДУЕТ делать строки длиннее 80 символов; те строки, что длиннее, СЛЕДУЕТ разбивать на несколько строк, не более 80 символов в каждой.

НЕДОПУСТИМО оставлять пробелы в конце непустых строк.

МОЖНО использовать пустые строки для улучшения читаемости и обозначения связанных блоков кода.

НЕДОПУСТИМО писать на одной строке более одной инструкции.

### Отступы

В коде НЕОБХОДИМО использовать отступ в 4 пробела и НЕДОПУСТИМО применять табы для отступов.

### Ключевые слова и True/False/Null

[Ключевые слова](https://www.php.net/manual/ru/reserved.keywords.php) PHP НЕОБХОДИМО писать в нижнем регистре.

Константы PHP: `true`, `false` и `null` — НЕОБХОДИМО писать в нижнем регистре.

### Типы

НЕОБХОДИМО использовать короткую форму типов, например `bool` вместо `boolean`, `int` вместо `integer` и т.д.

### Константы

Константам классов НЕОБХОДИМО давать имена в верхнем регистре с символом подчёркивания в качестве разделителя.

Пример:

```php
<?
namespace Vendor\Model;

class Foo
{
    const VERSION = '1.0';
    const DATE_APPROVED = '2012-06-01';
}
```

### Переменные

Для именования переменных СЛЕДУЕТ использовать стиль `camelCase`.

```php
$fooBar = null;
```

### Массивы

Массивы, которые записываются в несколько строк, СЛЕДУЕТ форматировать следующим образом:

```php
$arFilter = array(
    "key1" => "value1",
    "key2" => array(
        "key21" => "value21",
        "key22" => "value22",
    )
);
```

### Имена классов

Имена пространств имён и имена классов ДОЛЖНЫ следовать стандарту [PSR-0](https://www.php-fig.org/psr/psr-0/). В конечном итоге это означает, что каждый класс должен располагаться в отдельном файле и в пространстве имён с хотя бы одним верхним уровнем (именем производителя).

Имена классов ДОЛЖНЫ быть объявлены с использованием т.н. «`StudlyCaps`» (каждое слово начинается с большой буквы, между словами нет разделителей).

### Экземпляры классов

При создании экземпляра класса круглые скобки ДОЛЖНЫ присутствовать всегда, даже если в конструктор не переданы аргументы.

```php
new Foo();
```

### Пространства имён и оператор use

При наличии пространства имён (`namespace`), после его объявления необходимо оставить одну пустую строку.

При наличии операторов `use`, НЕОБХОДИМО располагать их после объявления пространства имён.

НЕОБХОДИМО использовать один оператор `use` на одно объявление (импорт или создание псевдонима).

НЕОБХОДИМО оставлять одну пустую строку после блока операторов `use`.

Например:

```php
<?
namespace Vendor\Package;

use FooClass;
use BarClass as Bar;
use OtherVendor\OtherPackage\BazClass;

// ... PHP код ...
```

### Extends и implements

Ключевые слова extends и implements НЕОБХОДИМО располагать на одной строке с именем класса.

Открывающую фигурную скобку класса НЕОБХОДИМО переносить на следующую строку; закрывающую фигурную скобку класса НЕОБХОДИМО располагать на следующей строке после тела класса.

```php
<?
namespace Vendor\Package;

use FooClass;
use BarClass as Bar;
use OtherVendor\OtherPackage\BazClass;

class ClassName extends ParentClass implements \ArrayAccess, \Countable
{
    // константы, свойства, методы
}
```

Список implements МОЖНО разбить на несколько строк, каждая из которых с одним отступом. При этом первый интерфейс в списке НЕОБХОДИМО перенести на следующую строку, и в каждой строке НЕОБХОДИМО указать только один интерфейс.

```php
<?
namespace Vendor\Package;

use FooClass;
use BarClass as Bar;
use OtherVendor\OtherPackage\BazClass;

class ClassName extends ParentClass implements
    \ArrayAccess,
    \Countable,
    \Serializable
{
    // константы, свойства, методы
}
```

### Свойства

Допустимые варианты стилей для имен свойств:

```php
$StudlyCaps = null;
$camelCase = null;
$under_score = null;
```

Предпочтительно использовать `camelCase`.

Какое бы соглашение об именовании не использовалось, его СЛЕДУЕТ последовательно применять ко всей соответствующей области кода. Эта область может быть представлена уровнем разработчика, пакета, класса или метода.

Видимость НЕОБХОДИМО объявлять для всех свойств.

Использовать ключевое слово var для объявления свойств НЕДОПУСТИМО.

НЕДОПУСТИМО в одном объявлении указывать более одного свойства.

НЕ СЛЕДУЕТ начинать название свойства с подчёркивания для обозначения приватной или защищённой видимости.

Объявление свойства выглядит как показано ниже.

```php
<?
namespace Vendor\Package;

class ClassName
{
    public $fooBar = null;
}
```

### Методы

Методам НЕОБХОДИМО давать имена в стиле `camelCase()`.

Видимость НЕОБХОДИМО объявлять для всех методов.

НЕ СЛЕДУЕТ начинать название метода с подчёркивания для обозначения приватной или защищённой видимости.

НЕДОПУСТИМО объявлять методы с пробелом после названия метода. Открывающую фигурную скобку НЕОБХОДИМО располагать на отдельной строке; закрывающую фигурную скобку НЕОБХОДИМО располагать на следующей строке после тела метода. НЕДОПУСТИМО оставлять пробел после открывающей круглой скобки и перед закрывающей.

Объявление метода выглядит как показано ниже. Обратите внимание на расположение запятых, пробелов, круглых, квадратных и фигурных скобок:

```php
<?
namespace Vendor\Package;

class ClassName
{
    public function fooBarBaz($arg1, &$arg2, $arg3 = [])
    {
        // тело метода
    }
}
```

### Аргументы методов

В списке аргументов НЕДОПУСТИМЫ пробелы перед запятыми и НЕОБХОДИМ один пробел после каждой запятой.

Аргументы метода со значениями по умолчанию НЕОБХОДИМО располагать в конце списка аргументов.

```php
<?
namespace Vendor\Package;

class ClassName
{
    public function foo($arg1, &$arg2, $arg3 = [])
    {
        // тело метода
    }
}
```

Список аргументов МОЖНО разбить на несколько строк, каждая из которых с одним отступом. При этом первый аргумент в списке НЕОБХОДИМО перенести на следующую строку, и в каждой строке НЕОБХОДИМО указать только один аргумент.

Когда список аргументов разбит на несколько строк, закрывающую круглую скобку и открывающую фигурную НЕОБХОДИМО располагать на одной отдельной строке, с одним пробелом между ними.

```php
<?
namespace Vendor\Package;

class ClassName
{
    public function aVeryLongMethodName(
        ClassTypeHint $arg1,
        &$arg2,
        array $arg3 = []
    ) {
        // тело метода
    }
}
```

При передаче аргумента по ссылке, пробелы между оператором `&` и аргументом НЕДОПУСТИМЫ

Также НЕДОПУСТИМЫ пробелы между оператором `...` и аргументом

```php
public function process(&$algorithm, ...$parts)
{
    // тело метода
}
```

### abstract, final и static

При наличии ключевых слов `abstract` и `final`, НЕОБХОДИМО чтобы они предшествовали модификаторам видимости.

При наличии ключевого слова `static`, НЕОБХОДИМО чтобы оно следовало за модификатором видимости.

```php
<?
namespace Vendor\Package;

abstract class ClassName
{
    protected static $foo;

    abstract protected function zim();

    final public static function bar()
    {
        // тело метода
    }
}
```

### Вызовы функций и методов

При вызове метода или функции НЕДОПУСТИМЫ пробелы между названием метода или функции и открывающей круглой скобкой, а также НЕДОПУСТИМЫ пробелы после открывающей круглой скобки и перед закрывающей. В списке аргументов НЕДОПУСТИМЫ пробелы перед запятыми, и НЕОБХОДИМ один пробел после каждой запятой.

```php
<?
bar();
$foo->bar($arg1);
Foo::bar($arg2, $arg3);
```

Список аргументов МОЖНО разбить на несколько строк, каждая из которых с одним отступом. При этом первый аргумент в списке НЕОБХОДИМО перенести на следующую строку, и в каждой строке НЕОБХОДИМО указать только один аргумент.

```php
<?
$foo->bar(
    $longArgument,
    $longerArgument,
    $muchLongerArgument
);
```

Один аргумент, разбитый на несколько строк (как это может быть в случае с анонимной функцией или массивом), не представляет собой разбиение списка аргументов.

```php
<?
somefunction($foo, $bar, [
  // ...
], $baz);

$app->get('/hello/{name}', function ($name) use ($app) {
    return 'Hello ' . $app->escape($name);
});
```

### Управляющие структуры

Общие правила стиля для управляющих структур таковы:

- после ключевого слова управляющей структуры НЕОБХОДИМ один пробел;
- после открывающей круглой скобки пробел НЕДОПУСТИМ;
- перед закрывающей круглой скобкой пробел НЕДОПУСТИМ;
- НЕОБХОДИМ один пробел между закрывающей круглой скобкой и открывающей фигурной скобкой;
- тело управляющей структуры НЕОБХОДИМО смещать на один отступ;
- закрывающую фигурную скобку НЕОБХОДИМО располагать на следующей строке после тела.

Тело каждой управляющей структуры НЕОБХОДИМО заключать в фигурные скобки. Это стандартизирует вид управляющих структур и уменьшает вероятность возникновения ошибок при добавлении новых строк в тело.

### if, elseif, else

Управляющая структура `if` выглядит, как показано ниже. Обратите внимание на расположение пробелов, круглых и фигурных скобок, а также, что `else` и `elseif` расположены на одной строке с закрывающей фигурной скобкой предыдущего тела.

```php
<?
if ($expr1) {
    // тело if
} elseif ($expr2) {
    // тело elseif
} else {
    // тело else;
}
```

Вместо `else if` СЛЕДУЕТ использовать `elseif`, чтобы все ключевые слова управляющих структур выглядели как одно слово.

Выражение в скобках МОЖНО разбить на несколько строк, каждая из которых с одним отступом. При этом первое условие НЕОБХОДИМО перенести на следующую строку. Закрывающую круглую скобку и открывающую фигурную НЕОБХОДИМО располагать на одной отдельной строке, с одним пробелом между ними. НЕОБХОДИМО, чтобы булевы операторы между условиями всегда были в начале или в конце строки, но не оба варианта.

```php
<?
if (
    $expr1
    && $expr2
) {
    // тело if
} elseif (
    $expr3
    && $expr4
) {
    // тело elseif
}
```

### switch, case

Управляющая структура `switch` выглядит, как показано ниже. Обратите внимание на расположение пробелов, круглых и фигурных скобок: выражение `case` НЕОБХОДИМО смещать на один отступ от `switch`, а ключевое слово `break` (или другое завершающее ключевое слово) НЕОБХОДИМО смещать на тот же уровень, что и тело `case`. При умышленном проваливании из непустого case НЕОБХОДИМ комментарий вроде `// no break`.

```php
<?
switch ($expr) {
    case 0:
        echo 'Первый case, заканчивается на break';
        break;
    case 1:
        echo 'Второй case, с умышленным проваливанием';
        // no break
    case 2:
    case 3:
    case 4:
        echo 'Третий case, завершается словом return вместо break';
        return;
    default:
        echo 'По умолчанию';
        break;
}
```

Выражение в скобках МОЖНО разбить на несколько строк, каждая из которых с одним отступом. При этом первое условие НЕОБХОДИМО перенести на следующую строку. Закрывающую круглую скобку и открывающую фигурную НЕОБХОДИМО располагать на одной отдельной строке, с одним пробелом между ними. НЕОБХОДИМО, чтобы булевы операторы между условиями всегда были в начале или в конце строки, но не оба варианта.

```php
<?
switch (
    $expr1
    && $expr2
) {
    // тело switch
}
```

### while, do while

Управляющая структура `while` выглядит, как показано ниже. Обратите внимание на расположение пробелов, круглых и фигурных скобок:

```php
<?
while ($expr) {
    // тело while
}
```

Аналогично, управляющая структура `do while` выглядит, как показано ниже. Обратите внимание на расположение пробелов, круглых и фигурных скобок:

```php
<?
do {
    // тело do while
} while ($expr);
```

Выражение в скобках в структурах `while` и `do while` МОЖНО разбить на несколько строк аналогично структурам `if` и `switch`

### for

Управляющая структура `for` выглядит, как показано ниже. Обратите внимание на расположение пробелов, круглых и фигурных скобок:

```php
<?
for ($i = 0; $i < 10; $i++) {
    // тело for
}
```

Выражение в скобках МОЖНО разбить на несколько строк, каждая из которых с одним отступом. При этом первое условие НЕОБХОДИМО перенести на следующую строку. Закрывающую круглую скобку и открывающую фигурную НЕОБХОДИМО располагать на одной отдельной строке, с одним пробелом между ними.

```php
<?
for (
    $i = 0;
    $i < 10;
    $i++
) {
    // тело for
}
```

### foreach

Управляющая структура `foreach` выглядит, как показано ниже. Обратите внимание на расположение пробелов, круглых и фигурных скобок:

```php
<?
foreach ($iterable as $key => $value) {
    // тело foreach
}
```

### try, catch

Блок `try catch` выглядит, как показано ниже. Обратите внимание на расположение пробелов, круглых и фигурных скобок:

```php
<?
try {
     // тело try
} catch (FirstExceptionType $e) {
    // тело catch
} catch (OtherExceptionType $e) {
    // тело catch
}
```

### Операторы

Если вокруг оператора разрешены пробелы, МОЖНО использовать несколько пробелов для удобства чтения.

#### Унарные операторы

НЕДОПУСТИМЫ пробелы между оператором инкремента/декремента и операндом.

```php
$i++;
++$j;
```

Для оператора приведения типов НЕДОПУСТИМЫ пробелы внутри скобок

```php
$intValue = (int) $input;
```

#### Бинарные операторы

Все бинарные операторы ([арифметические](https://www.php.net/manual/ru/language.operators.arithmetic.php), [сравнения](https://www.php.net/manual/ru/language.operators.comparison.php), [присваивания](https://www.php.net/manual/ru/language.operators.assignment.php), [побитовые](https://www.php.net/manual/ru/language.operators.bitwise.php), [логические](https://www.php.net/manual/ru/language.operators.logical.php), [строковые](https://www.php.net/manual/ru/language.operators.string.php), [проверки типа](https://www.php.net/manual/ru/language.operators.type.php)) ДОЛЖНЫ быть отделены хотя бы одним пробелом с обеих сторон

```php
if ($a === $b) {
    $foo = $bar ?? $a ?? $b;
} elseif ($a > $b) {
    $foo = $a + $b * $c;
}
```

#### Тернарные операторы

Внутри тернарного оператора, вокруг символов `?` и `:` ДОЛЖЕН быть хотя бы один пробел:

```php
$variable = $foo ? 'foo' : 'bar';
```

Когда средний операнд отсутствует, оператор ДОЛЖЕН следовать тем же правилам стиля, что и другие бинарные операторы:

```php
$variable = $foo ?: 'bar';
```

### Замыкания

В объявлении замыкания НЕОБХОДИМ пробел после ключевого слова `function`, а также до и после ключевого слова `use`.

Открывающую фигурную скобку НЕОБХОДИМО располагать на той же строке; закрывающую фигурную скобку НЕОБХОДИМО располагать на следующей строке после тела замыкания.

НЕДОПУСТИМЫ пробелы после открывающей круглой скобки списка аргументов или переменных, и НЕДОПУСТИМЫ пробелы перед закрывающей круглой скобкой списка аргументов или переменных.

В списке аргументов и в списке переменных НЕДОПУСТИМЫ пробелы перед запятыми, и НЕОБХОДИМ один пробел после каждой запятой.

Аргументы замыкания со значениями по умолчанию НЕОБХОДИМО располагать в конце списка аргументов.

Объявление замыкания выглядит, как показано ниже. Обратите внимание на расположение запятых, пробелов, круглых, квадратных и фигурных скобок:

```php
<?
$closureWithArgs = function ($arg1, $arg2) {
    // тело
};

$closureWithArgsAndVars = function ($arg1, $arg2) use ($var1, $var2) {
    // тело
};
```

Список аргументов и список переменных МОЖНО разбить на несколько строк, каждая из которых с одним отступом. При этом первый элемент в списке НЕОБХОДИМО перенести на следующую строку, и в каждой строке НЕОБХОДИМО указать только один аргумент или одну переменную.

Когда последний список (аргументов, или переменных) разбит на несколько строк, закрывающую круглую скобку и открывающую фигурную НЕОБХОДИМО располагать на одной отдельной строке, с одним пробелом между ними.

Ниже показаны примеры замыканий с аргументами и без, а также со списком свободных переменных на несколько строк.

```php
<?
$longArgs_noVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) {
   // тело
};

$noArgs_longVars = function () use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
   // тело
};

$longArgs_longVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
   // тело
};

$longArgs_shortVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) use ($var1) {
   // тело
};

$shortArgs_longVars = function ($arg) use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
   // тело
};
```

Обратите внимание, правила форматирования так же применяются, когда замыкание используется напрямую в качестве аргумента при вызове метода или функции.

```php
<?
$foo->bar(
    $arg1,
    function ($arg2) use ($var1) {
        // тело
    },
    $arg3
);
```
